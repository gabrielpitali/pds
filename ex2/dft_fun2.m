function Xk=dft_fun2(xn)
  N = length(xn);
  disp(N);
  for k=0:N-1
    disp(k);
    for n=0:N-1
      Wn=exp(-j*2*pi*k*n/N);
      X1(k+1,n+1)=Wn;
    endfor
  endfor
  Xk=X1*xn';
endfunction
