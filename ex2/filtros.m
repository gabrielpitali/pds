close all;
clear all;
clc;

% Filtro de m�dia m�vel N = 3
N = 3;
h = 1/N*ones(N,1);
w = -pi:pi/500:pi;
H = freqz(h,1,w);
figure(1);
subplot(3,1,1);
plot(w,abs(H));
xlabel('Frequency');
ylabel('Magnitude');
title('N = 3');

% Filtro de m�dia m�vel N = 10
N = 10;
h = 1/N*ones(N,1);
w = -pi:pi/500:pi;
H = freqz(h,1,w);
subplot(3,1,2);
plot(w,abs(H));
xlabel('Frequency');
ylabel('Magnitude');
title('N = 10');

% Filtro de m�dia m�vel N = 100
N = 100;
h = 1/N*ones(N,1);
w = -pi:pi/500:pi;
H = freqz(h,1,w);
subplot(3,1,3);
plot(w,abs(H));
xlabel('Frequency');
ylabel('Magnitude');
title('N = 100');